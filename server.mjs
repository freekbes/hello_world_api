// allow require in ES6 module to import json files
import { createRequire } from "module";
const require = createRequire(import.meta.url);

import express from 'express';

let config;
try {
	config = require('./config.json');
}
catch (err) {
	console.error("Could not find config.json file in current directory. Unable to start the API.");
	process.exit(2);
}

async function startWebServer(port) {
	const exprServer = express();

	exprServer.get('/hello_world', function(req, res) {
		console.log('Incoming request for /hello_world from ' + req.ip);
		const data = { 'result': 'hello_world' };
		res.json(data);
	});

	const server = await exprServer.listen(port);
	console.log('Server has started on port ' + port);
	return (server);
}

startWebServer(config['port']);
