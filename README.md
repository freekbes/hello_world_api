# Hello World API

## Installation

Clone into `/opt` folder: `git clone https://gitlab.com/freekbes/hello_world_api.git /opt/hello_world_api`

Copy `config.json.example` to `config.json` to apply a configuration for the API's webserver. Here you can modify the port it runs on, which is 5000 by default.

Run `npm install` before starting the server for the first time.

Move all `*.service` and `*.timer` files into `/etc/systemd/system`.

## Starting the server

Start the server with `systemctl start helloworldapi`

Start on boot with `systemctl enable helloworldapi`

Update at 2:05 AM every night with `systemctl enable helloworldupdate.timer` (don't forget to `start` as well)
